package com.tc.itfarm.model.ext;

import com.tc.itfarm.model.Comment;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wangdongdong on 2016/9/20.
 */
public class CommentVO implements Serializable {

    private Comment comment;
    /*子评论*/
    private List<CommentVO> child;

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public List<CommentVO> getChild() {
        return child;
    }

    public void setChild(List<CommentVO> child) {
        this.child = child;
    }
}
