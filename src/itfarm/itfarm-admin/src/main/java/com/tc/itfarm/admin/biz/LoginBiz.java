package com.tc.itfarm.admin.biz;

import com.tc.itfarm.model.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;

@Service
public class LoginBiz {
	
	/**
	 * 获取当前登陆用户
	 * @author: wangdongdong
	 * @date: 2016年7月13日       
	 * @return
	 */
	public User getCurUser() {
		return (User) SecurityUtils.getSubject().getPrincipal();
	}

	/**
     * 登出用户
	 */
	public void userLoginOut() {
		Subject subject = SecurityUtils.getSubject();
		if (subject.isAuthenticated()) {
			subject.logout();
		}
	}
}
