<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<%@ include file="/WEB-INF/layouts/base_style.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title> ${config.webTitle} </title>
    <meta charset="utf-8">
    <script type="text/javascript">
        $(function () {
            $(".cat_item_img").zoomImgRollover();
            $('#more_article').click(function () {
                var pageIndex = parseInt($('#more_article').attr('pageNo'));
                if ((pageIndex + 1) == ${page.totalPages}) {
                    $('#more_article').text("没有更多了");
                    $('#more_article').attr('disabled', 'disabled');
                }
                $.post("${ctx}/article/nextPage.do",
                        {
                            pageNo: pageIndex + 1
                        },
                        function (data) {
                            $('#content2').append(data);
                            $('#more_article').attr('pageNo', pageIndex + 1);
                        });
            });
        })
    </script>
</head>
<body>
<jsp:include page="../index/header.jsp"/>
<div id="main_content" class="container">

    <div class="col-md-8">
        <!--slider-->
        <div class="col-md-12 panel panel-info" style="padding: 0">
            <div id="myCarousel" class="carousel slide">
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                    <li data-target="#myCarousel" data-slide-to="4"></li>
                    <li data-target="#myCarousel" data-slide-to="5"></li>
                </ol>
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="active item img-rounded"
                         style="background: url('${ctx}/images/hello_world.jpg'); background-size: cover">
                        <div class="slider_photo">
                            <div class="slider_title">
                                <a style="color:white; font-size: 30px;" href="#">${config.webTitle}</a>
                            </div>
                        </div>
                    </div>
                    <c:forEach items="${newArticles }" var="item">
                        <div class="item img-rounded"
                             style="background: url('${ctx}/titleImg/${item.article.titleImg}'); background-size: cover">
                            <div class="slider_photo">
                                <div class="slider_title">
                                    <a style="color:white; font-size: 30px;"
                                       href="${ctx }/article/456${item.article.recordId}.html">${item.article.title}</a>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <!-- Carousel nav -->
                <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
            </div>
        </div>
        <!--top page-->
        <div class="col-md-12 panel panel-info">


            <h2><a href="#">印象代码正在开发中。。。。。。</a><span class="glyphicon glyphicon-thumbs-up"></span></h2>

            <p class="meta">
                <span class="glyphicon glyphicon-user"></span>&nbsp;<a style="font-size: 10px;">王东东</a>&nbsp;&nbsp;
                <span class="glyphicon glyphicon-time"></span>&nbsp;<a style="font-size: 10px;">2016年8月20日</a>&nbsp;&nbsp;
                <span class="glyphicon glyphicon-comment"></span>&nbsp;<a style="font-size: 10px;">20</a>&nbsp;&nbsp;
                <span class="glyphicon glyphicon-eye-open"></span>&nbsp;<a style="font-size: 10px;">356次浏览</a>&nbsp;&nbsp;
                <span class="glyphicon glyphicon-folder-open"></span>&nbsp;<a style="font-size: 10px;">java</a>&nbsp;&nbsp;
                <span class="glyphicon glyphicon-tag"></span>&nbsp;<a style="font-size: 10px;">数据结构</a>&nbsp;&nbsp;
            </p>

            <hr/>

            <p>从前端到后台，完全由个人开发的网站，一直更新</p>
        </div>
        <c:forEach items="${list}" var="item">
            <c:if test="${item.count > 0}">
                <div class="col-md-12 panel panel-default" style="padding: 0">
                    <div class="panel-heading" style="border:transparent">
                        <a>${item.menu.name}</a><span style="float: right"><span
                            class="glyphicon glyphicon-list-alt"></span>&nbsp;${item.count}篇文章</span>
                    </div>
                    <div class="panel-body fly-reverse" style="padding: 10px;">
                        <c:forEach items="${item.articles}" var="a">
                            <div class="col-md-3 panel cat_item" style="padding: 10px;">
                                <div style="width: 100%; margin: auto;">
                                    <img class="cat_item_img" src="${ctx}/titleImg/${a.article.titleImg}"
                                         onerror="this.src='${ctx }/images/hello_world.jpg'" alt="" width="100%"
                                         height="117">
                                </div>
                                <div style="width: 100%">
                                    <a href="${ctx }/article/456${a.article.recordId}.html">${a.article.title}</a>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </c:if>
        </c:forEach>
        <div class="col-md-12 panel panel-default" style="padding: 0">
            <div class="panel-heading">
                最新文章
            </div>
            <div id="content2" class="panel-body fly-reverse">
                <c:forEach items="${articles }" var="item" varStatus="vs">
                    <div class="panel-info" style="height: 250px;">
                        <div class="col-md-4 panel" style="height: 100%; text-align: center">
                            <div style="width: 220px; height: 180px; margin-top: 30px; text-align: center; overflow: hidden">
                                <img src="${ctx}/titleImg/${item.article.titleImg}"
                                     onerror="this.src='${ctx }/images/hello_world.jpg'" alt="" width="220" height="160"
                                     style="border: 1px solid #ddd;padding: 5px;background: #fff;">
                            </div>
                        </div>
                        <div class="col-md-8 panel" style="height: 250px;">
                            <h2><a href="${ctx }/article/456${item.article.recordId}.html">${item.article.title}</a>
                            </h2>

                            <p class="meta">
                                <span class="glyphicon glyphicon-user"></span>&nbsp;<a
                                    style="font-size: 10px;">${item.authorName}</a>&nbsp;&nbsp;
                                <span class="glyphicon glyphicon-time"></span>&nbsp;<a
                                    style="font-size: 10px;">${item.lastDate}</a>&nbsp;&nbsp;
                                <span class="glyphicon glyphicon-comment"></span>&nbsp;<a
                                    style="font-size: 10px;">${item.commentCount}</a>&nbsp;&nbsp;
                                <span class="glyphicon glyphicon-eye-open"></span>&nbsp;<a
                                    style="font-size: 10px;">${item.article.pageView}</a>&nbsp;&nbsp;
                                <span class="glyphicon glyphicon-folder-open"></span>&nbsp;<a
                                    style="font-size: 10px;">${item.categoryName}</a>&nbsp;&nbsp;
                                <span class="glyphicon glyphicon-tag"></span>&nbsp;<a
                                    style="font-size: 10px;">${item.article.keyword}</a>&nbsp;&nbsp;
                            </p>

                            <hr/>
                            <p><itfarm:StringCut length="200"
                                                 strValue="${item.article.content }"></itfarm:StringCut></p>
                        </div>
                    </div>
                </c:forEach>
            </div>
            <div style="height: 50px; text-align: center">
                <c:if test="${page.hasNextPage}">
                    <button type="button" id="more_article" class="btn btn-default" pageNo="${page.pageIndex}">查看更多
                    </button>
                </c:if>
                <c:if test="${!page.hasNextPage}">
                    <button type="button" class="btn btn-default">没有更多了</button>
                </c:if>
            </div>
        </div>
        </div>
        <jsp:include page="../index/right.jsp"></jsp:include>
    </div>
    <%--底部--%>
    <jsp:include page="../index/footer.jsp"/>
</body>
</html>